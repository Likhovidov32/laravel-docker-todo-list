<?php


namespace App\Services\Payment;


use App\Contracts\Payment\Payment;
use Illuminate\Support\Facades\Http;
use Illuminate\Support\Facades\Log;
use Mockery\Exception;
use PayFast\Auth;
use PayFast\PayFastApi;
use PayFast\PayFastPayment;

class Payfast implements Payment
{

    private $merchant;

    public function  __construct()
    {
        $this->merchant = [
            'id' => env('PAYFAST_MERCHANT_ID', ''),
            'key' => env('PAYFAST_MERCHANT_KEY', ''),
            'passphrase' => env('PAYFSAT_PASSPHRASE', ''),
            'testMode' => (bool) env('PAYFSAT_TESTMOD', false),
        ];
    }

    public function createCardToken($cardId): string
    {
        try {
            $payfast = new PayFastPayment(
                [
                    'merchantId' => $this->merchant['id'],
                    'merchantKey' => $this->merchant['key'],
                    'passPhrase' => $this->merchant['passphrase'],
                    'testMode' => $this->merchant['testMode']
                ]
            );

            $data = [
                'amount' => '0.00',
                'item_name' => 'Add Card',
                'm_payment_id' => $cardId,
                'subscription_type' => '2',
//                'return_url' => route('payfast.success'),
//                'cancel_url' => route('payfast.cancel'),
//                'notify_url' => route('payfast.notify'),
            ];

            $data['amount'] = number_format( sprintf( '%.2f', $data['amount'] ), 2, '.', '' );

            $data = ['merchant_id' => PayFastPayment::$merchantId, 'merchant_key' => PayFastPayment::$merchantKey] + $data;

            $signature = Auth::generateSignature($data, PayFastPayment::$passPhrase);

            $payFastUrl = PayFastPayment::$baseUrl.'/eng/process';

            $response = Http::asForm()->post($payFastUrl, [
                'merchant_id' => $this->merchant['id'],
                'merchant_key' => $this->merchant['key'],
                'amount' => $data['amount'],
                'item_name' => $data['item_name'],
                'm_payment_id' => $data['m_payment_id'],
                'subscription_type' => $data['subscription_type'],
//                'return_url' => $data['return_url'],
//                'cancel_url' => $data['cancel_url'],
//                'notify_url' => $data['notify_url'],
                'signature' => $signature,
            ]);

            return $response->handlerStats()['url'];
        } catch(Exception $e) {
            echo 'There was an exception: '.$e->getMessage();
        }
    }

    public function checkoutByToken($token, $amount, $orderId): bool
    {
        $api = new PayFastApi(
            [
                'merchantId' => $this->merchant['id'],
                'merchantKey' => $this->merchant['key'],
                'passPhrase' => $this->merchant['passphrase'],
                'testMode' => $this->merchant['testMode']
            ]
        );

        $adhocResponse = $api->subscriptions->adhoc($token, ['amount' => ($amount * 100), 'item_name' => 'Order#' . $orderId]);

        return $adhocResponse;
    }
}
