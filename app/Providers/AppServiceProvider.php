<?php

namespace App\Providers;

use App\Contracts\Payment\Payment;
use App\Services\Payment\Payfast;
use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->singleton(Payment::class, function ($app) {
            return new Payfast();
        });
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }
}
