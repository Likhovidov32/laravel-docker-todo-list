<?php

namespace App\Listeners;

use App\Events\TaskDone;
use App\Mail\TaskDoneMail;
use App\Models\Task;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Support\Facades\Mail;

class SendMailTaskDone
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  \App\Events\TaskDone  $event
     * @return void
     */
    public function handle(TaskDone $event)
    {
        Mail::to(env("ADMIN_MAIL", "test@test.com"))->queue(new TaskDoneMail(Task::find($event->task->id)));
    }
}
