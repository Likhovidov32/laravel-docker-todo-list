<?php

namespace App\Http\Controllers\Api;

use App\Contracts\Payment\Payment;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Str;

class PaymentController extends Controller
{
    public function createCardToken(Request $request, Payment $payment)
    {
        // create logic to add to database

        return $payment->createCardToken(Str::random());
    }
}
