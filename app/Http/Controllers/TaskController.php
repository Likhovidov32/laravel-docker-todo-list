<?php

namespace App\Http\Controllers;

use App\Events\TaskCreated;
use App\Events\TaskDone;
use App\Events\TaskRemoved;
use App\Http\Requests\TaskRequest;
use App\Http\Resources\TaskCollection;
use App\Http\Resources\TaskResource;
use App\Models\Task;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;

class TaskController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return TaskCollection
     */
    public function index()
    {
        return new TaskCollection(Task::orderBy('id', 'DESC')->get()    );
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return TaskResource
     */
    public function store(TaskRequest $request)
    {
        $task = Task::create($request->validated());

        broadcast(new TaskCreated($task));

        return new TaskResource($task);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Task  $task
     * @return TaskResource
     */
    public function update(Request $request, Task $task)
    {
        $task->update(['completed' => $request->completed]);

        broadcast(new TaskDone($task));

        return new TaskResource($task);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Task  $task
     * @return \Illuminate\Http\Response
     */
    public function destroy(Task $task)
    {
        broadcast(new TaskRemoved($task));

        $task->delete();

        return response()->noContent();
    }
}
