<?php

namespace App\Contracts\Payment;

interface Payment {
    public function createCardToken($cardId): string;
    public function checkoutByToken($token, $amount, $orderId);
}
