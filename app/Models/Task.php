<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Task extends Model
{
    use HasFactory;

    protected $fillable = ['title','completed'];

    protected $attributes = [
        'completed' => 0,
    ];

    protected $hidden = [
        'created_at',
        'updated_at'
    ];
}
