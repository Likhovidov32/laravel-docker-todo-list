## Set up

- cp .env.example .env
- composer install
- npm install
- php artisan key:generate
- php artisan jwt:secret
- .env
    - set pusher account
    - set email smtp
    - set db connection
    - set broadcast pusher
    - set queue database
    - set ADMIN_MAIL
    
- sudo make start
- sudo make docker-exec
- php artisan migrate
- php artisan db:seed
- php srtisan queue:work
