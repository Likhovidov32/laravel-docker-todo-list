<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Db::table('users')->insertOrIgnore([
            [
                'name' => 'user1',
                'username' => 'user1',
                'password' => Hash::make('user1'),
            ],
            [
                'name' => 'user2',
                'username' => 'user2',
                'password' => Hash::make('user2'),
            ],
        ]);
    }
}
