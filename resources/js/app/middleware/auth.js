export default function auth ({next, store}){
    let seconds = new Date().getTime()
    seconds = (seconds - Number.parseInt(localStorage.getItem('TokenStart'))) / 1000

    if(!store.state.auth.token)
    {
        return next({name: 'login'})
    }
    else if( seconds > Number.parseInt(localStorage.getItem('TokenExpiry')))
    {
        store.state.auth.token=null

        return next({name: 'login'})
    }

    return next()
}
