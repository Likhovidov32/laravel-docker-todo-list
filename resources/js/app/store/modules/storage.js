const BASE_URL = '/admin/api';
import axios from "axios";
function initialState () {
    const loader = false;
    return {
        loader
    }
}

const getters = {
};

const actions = {
    async getUserReport(ctx, data)
    {
        return new Promise((resolve, reject) => {
            axios({
                url: BASE_URL + '/users/report?page=' + data.page + '&orderBy=' + data.orderBy + '&orderByMethod=' + data.orderByMethod,
                method: 'GET',
                headers: {
                    'Content-Type': 'application/json',
                    'Authorization': 'Bearer ' + localStorage.getItem('access_token')
                },
            })
                .then((response) => {
                    resolve(response)
                })
                .catch((error) => {
                    reject(error)
                })
        })
    },
    async getUserReportDownload(ctx, data)
    {
        return new Promise((resolve, reject) => {
            axios({
                url: BASE_URL + '/users/report/download',
                method: 'GET',
                responseType: 'blob',
                headers: {
                    'Content-Type': 'application/json',
                    'Authorization': 'Bearer ' + localStorage.getItem('access_token')
                },
            })
                .then((response) => {
                    const downloadUrl = window.URL.createObjectURL(new Blob([response]));

                    const link = document.createElement('a');

                    link.href = downloadUrl;

                    link.setAttribute('download', 'users.csv'); //any other extension

                    document.body.appendChild(link);

                    link.click();

                    link.remove();
                    resolve(response)
                })
                .catch((error) => {
                    reject(error)
                })
        })
    },
};

const mutations = {

};

export default {
    namespaced: true,
    state: initialState,
    getters,
    actions,
    mutations
}
