const BASE_URL = '/api';
import axios from "axios";
function initialState () {
    const token = localStorage.getItem('access_token') || "";
    const isAuth = token !== "";
    const loader = false;
    return {
        token,
        isAuth,
        loader
    }
}

const getters = {
};

const actions = {
    async login(ctx, data)
    {
        return new Promise((resolve, reject) => {
            axios({
                url: BASE_URL + '/login',
                method: 'POST',
                data: data
            })
            .then((response) => {
                ctx.commit('setToken', response.data.access_token)
                ctx.commit('setIsAuth', true)
                localStorage.setItem('TokenStart', new Date().getTime());
                localStorage.setItem('TokenExpiry', response.data.expires_in);
                resolve(response)
            })
            .catch((error) => {
                reject(error)
            })
        })
    },
    async logout(ctx)
    {
        ctx.commit('setToken', "")
        ctx.commit('setIsAuth', '')
    }
};

const mutations = {
    setToken(state, token)
    {
        state.token = token;
        localStorage.setItem('access_token', state.token);
    },
    setIsAuth(state)
    {
        state.isAuth = state.token !== ""
    },
};

export default {
    namespaced: true,
    state: initialState,
    getters,
    actions,
    mutations
}
