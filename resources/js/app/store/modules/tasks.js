const BASE_URL = '/api';
import axios from "axios";
function initialState () {
    const loader = false;
    const todos =  [];
    const newTodo =  {
        title: '',
        completed: false
    };
    return {
        loader,
        todos,
        newTodo
    }
}

const getters = {
    newTask(state)
    {
        return state.newTodo
    },
    todos(state)
    {
        return state.todos
    },
    toRemove(state)
    {
        return state.toRemove
    },
    completed(state, todo)
    {
        return state.todos.filter(todo => todo.completed === 1)
    },
};

const actions = {
    async getTasks(ctx, data)
    {
        return new Promise((resolve, reject) => {
            axios({
                url: BASE_URL + '/task',
                method: 'GET',
                headers: {
                    'Content-Type': 'application/json',
                    'Authorization': 'Bearer ' + localStorage.getItem('access_token')
                },
                data: data
            })
                .then((response) => {
                    resolve(response)
                    ctx.commit('setTasksMutation', response.data.data)
                })
                .catch((error) => {
                    reject(error)
                })
        })
    },
    async addTask(ctx, data)
    {
        return new Promise((resolve, reject) => {
            axios({
                url: BASE_URL + '/task',
                method: 'POST',
                headers: {
                    'Content-Type': 'application/json',
                    'Authorization': 'Bearer ' + localStorage.getItem('access_token')
                },
                data: data
            })
                .then((response) => {
                    resolve(response)
                })
                .catch((error) => {
                    reject(error)
                })
        })
    },
    async updateTask(ctx, data)
    {
        return new Promise((resolve, reject) => {
            axios({
                url: BASE_URL + '/task/' + data.id,
                method: 'PUT',
                headers: {
                    'Content-Type': 'application/json',
                    'Authorization': 'Bearer ' + localStorage.getItem('access_token')
                },
                data: {
                    completed: 1
                }
            })
                .then((response) => {
                    resolve(response)
                })
                .catch((error) => {
                    reject(error)
                })
        })
    },
    async deleteTask(ctx, data)
    {
        return new Promise((resolve, reject) => {
            axios({
                url: BASE_URL + '/task/' + data.id,
                method: 'DELETE',
                headers: {
                    'Content-Type': 'application/json',
                    'Authorization': 'Bearer ' + localStorage.getItem('access_token')
                },
            })
                .then((response) => {
                    resolve(response)
                })
                .catch((error) => {
                    reject(error)
                })
        })
    },
};

const mutations = {
    setTasksMutation(state, tasks)
    {
        state.todos = tasks;
    },
    addTodoMutation(state, task)
    {
        state.todos.unshift(task)
    },
    deleteTodoMutation(state, task)
    {
        state.todos = state.todos.filter((element) => { return element.id !== task.id } )
    },
    completeTaskMutation(state, task)
    {
        state.todos = state.todos.map((element) => {
            if(element.id === task.id )
                return task

            return element
        } )
    },
};

export default {
    namespaced: true,
    state: initialState,
    getters,
    actions,
    mutations
}
