require('../bootstrap');

import Vue from 'vue'
import VueRouter from 'vue-router'
import Vuetify from 'vuetify'
import store from './store'
import 'vuetify/dist/vuetify.min.css'
Vue.use(VueRouter)
Vue.use(Vuetify);


import App from './App.vue'
import Login from '../components/Login.vue'
import Todo from '../components/Todo.vue'

import guest from "./middleware/guest";
import auth from "./middleware/auth";
import middlewarePipeline from "./middlewarePipeline";

const router = new VueRouter({
    mode: 'history',
    routes: [
        {
            path: '/login',
            name: 'login',
            component: Login,
            meta: {
                middleware: [
                    guest
                ]
            },
        },
        {
            path: '/',
            name: 'todo',
            component: Todo,
            meta: {
                layout: 'main',
                middleware: [
                    auth
                ]
            },
        },
    ],
});

router.beforeEach((to, from, next) => {
    if (!to.meta.middleware) {
        return next()
    }
    const middleware = to.meta.middleware
    const context = {
        to,
        from,
        next,
        store
    }
    return middleware[0]({
        ...context,
        next: middlewarePipeline(context, middleware, 1)
    })
})

const app = new Vue({
    vuetify : new Vuetify(),
    el: '#app',
    components: { App },
    router,
    store,
});
